## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
Javacript&Jquery desenvolvimento de interfaces web interativas. 
Introdução ao Angular.js. 
2. Quais foram os últimos dois framework javascript que você trabalhou?
Jquery e Angular.js (pequenas aplicações).
3. Descreva os principais pontos positivos de seu framework favorito.
Uma grande comunidade que ajudar e também um é fácil de aprender e rápido desensovilmento de aplicações.
4. Descreva os principais pontos negativos do seu framework favorito.
Acho que o problema de não ser um framework MVC, o que pode atrapalhar caso o projeto seja grande e desorganiza o código fácil.
Assim como é complicado realizar manutenção em uma situção de projetos grandes.
5. O que é código de qualidade para você?
Limpo, eficiente, reutilizavél e de fácil manutenção.
## Conhecimento de desenvolvimento
1. O que é GIT?
É um sistema de controle de versão.
2. Descreva um workflow simples de trabalho utilizando GIT.
Dá um fork no projeto, criar uma branch e quando terminar as modições dá um pull request para o master.
3. Como você avalia seu grau de conhecimento em Orientação a objeto?
Médio. Aplico às vezes, mas não sempre, ainda estou estudando e aplicando aos poucos.
4. O que é Dependency Injection?
Dependency Injection é basicamente, ir fornecendo os objetos que um objeto precisa (suas dependências) 
em vez de ter que construí-los em si, assim ajuda a tornar o código mais testável. 
5. O significa a sigla S.O.L.I.D?
Significa 5 principios para organização de código, que permitem o melhor aproveitamento, manutenção e testabilidade.
6. O que é BDD e qual sua importância para o processo de desenvolvimento?
É uma foram de desenvolver um código, separando por camadas e que permite uma melhor execução de um projeto.
7. Fale sobre Design Patterns.
Design patterns focam na reutilização de soluções para atender a problemas que podem surgir em um projeto, são padrões que se deve 
recorrer para chegar uma solução rápida.
9. Discorra sobre práticas que você considera boas no desenvolvimento javascript.
Organizar as váriaveis por ordem e importância. Separar o código por funções que podem ser reutilizadas. Comentar o código,
colocar significado a váriaveis, separar o código por partes.