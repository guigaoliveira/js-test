var IS_RESULT = false; // variaval global para saber se area de calcular é um resultado ou não
var LAST_VAL=0; // variaval global para saber o ultimo valor clicado
var CHECK_COMMA = false; // variaval global para checar se a virgula foi usada apenas uma vez em cada numero

/**
	Essa função é responsável por mostrar o resultado.
	Transforma a vírgula do texto em ponto e não for usado em vazio. 
	Então separa os números pelos operadores para verificar se o último número não é vazio.
	Transforma o texto em uma operação para o javacript executar.
	Se tudo estiver OK, então transforma o ponto em vírgula e atualiza o resultado.
	Se não, mostra "Error".
	O texto na area passa a ser um resultado.

**/
function calc_result(div, div_val)
{
	var no_comma = div_val.replace(",", ".");  
	var clean_result = no_comma.replace(/[^-\d/*+.]/g,''); 
	var result=0;

	try
	{
		result = eval(clean_result);
		if(!isFinite(result)) 
		{
			result = "Error"; 
			LAST_VAL='E';
		}
	} 
	catch(e) 
	{ 
		LAST_VAL='E';
		result ="Error"; 
	}

	result = result.toString().replace(".", ","); 
	div.html(result);  
	IS_RESULT = true; 
}
/** 

	Essa função é responsável por limpar a área de calcular.
	Se o td clicado for um número e a área de calcular estiver igual = 0 ou com algum resultado
	Então limpa a área. 
	Se o último valor tiver setado para 'E' e não for númerico, então a área de calcular passar a ser 0. 

**/
function clean_update(div, div_val, val)
{
	if($.isNumeric(val) && (div_val === '0' || IS_RESULT)) 
	{ 
		div.html('');
		IS_RESULT = false; 
	}
	if(!$.isNumeric(val) && LAST_VAL === 'E') 
	{
		div.html('0');
		LAST_VAL = '0';
		IS_RESULT = false; 
	}
	console.log(div_val);
}
/**

	Essa função adiciona um novo algarismo/operador a sentença matemática.
	Se o td clicado for Clear então a área de calcular passa a ser 0.
	Se o td for um número, então adiciona ele. 
	Se não, verificar se o último valor de td (operador/vírgula) é igual a valor atual.
	Se o valor for uma vírgula então checa se existe ou não dentro do número atual. 
	Se não existir, imprime. Se o último valor for um operador então adiciona o 0 na frente.
	Se não for uma vígula, se o último valor for um operador então substitui com o operador clicado.
	E atualiza a área de calcular. 
	Se o último valor não for um operador, adiciona o operador a área.

**/
function calc_add(div, div_val, val)
{
	if(val === "Clear")
	{
		div.html('0');
		CHECK_COMMA = false; 
	}
	else
	{
		if($.isNumeric(val)) 
		{
			div.append(val); 
		}
		else if(LAST_VAL!==val) 
		{
			var found_last = LAST_VAL.match(/[+\-\*\/]+/); // procura se o ultimo valor é um operador
			if(val===',') 
			{
				if(!CHECK_COMMA)
				{
					if(found_last) // adiciona 0 + a vírgula
					{
						val = '0'+val;
					}
					div.append(val); 
					CHECK_COMMA = true;
				}
			}
			else
			{
				if(found_last)
				{
					div_val = div_val.slice(0,-1) + val; // atualiza operador
					div.html(div_val); 
				}
				else
				{
					div.append(val); 
				}
				CHECK_COMMA = false; 
			}
		}
	}
	LAST_VAL = val.slice(-1); // pega o último valor clicado
	IS_RESULT = false; // não é um resultado
}

$(function()
{
	'use strict';
	$(".td-button").click(function()
	{
		var td_val = $(this).text(); 
		var calc_div = $(".calc-area");
		var calc_val = calc_div.text();

		clean_update(calc_div,calc_val,td_val); 

		if(td_val === '=') // igualdade, então mostrar o resultado
		{
			calc_result(calc_div,calc_val);
		}
		else
		{
			calc_add(calc_div,calc_val,td_val);
		}
	});
});