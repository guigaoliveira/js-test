$(function()
{
  var url_regex= (/(https?:\/\/.*\.(?:png|jpg))/i);
  var back_regex = (/^#(?:[0-9a-fA-F]{3}){1,2}$/g);
  var date_regex = (/^[0-3]?[0-9]\/[0-3]?[0-9]\/(?:[0-9]{2})?[0-9]{2}$/g);
  var url_id="#url-change";
  var back_id="#back-change";
  var date_id="#date-change";
  var s_id= "#session-";
  /* Função que verifica se os valores de um array (valores dos inputs) estão todos preenchidos */
  
  function inputs_empty(array)
  {
      for(var i = 0; i < array.length; i++) 
      {
          if(array[i].trim().length === 0) 
          {
              return true;
          }
      }
      return false;
  }

  /* Função que pega todos os valores de todos os inputs de uma div */

  function inputs_get(id)
  {
       return $(id+" input").map(function(){ return this.value; }).get();
  }

  /* Função que envia ao servidor um json para ele retorna uma parte da página que está sendo construida. */
  function new_bloc(order,json)
  {
      json = JSON.stringify(json);
      $.ajax({

            url: '/session_'+order, 
            type: 'POST', 
            processData: false,
            contentType: "application/json; charset=UTF-8",
            data: json,
            success: function(data) 
            {
                $("#drop-area").append(data);
                var json_add=$("#json-result").val().length;
                if(json>0)
                {
                   json=","+json;
                } 
                $("#json-result").append(json);
                $('html, body').animate({ scrollTop: $("#session-"+order).height() }, 500);
            }
        });
  }

  /* Função de arraste pela ordem de divs */
  function session_drag(order)
  {
     $(s_id+order).draggable(
     {
          helper: 'clone',
          revert: true,
          scroll:false,
          disabled: false,
          opacity: 0.8, 
          zIndex: 10000,
          drag: function(event,ui)
          {
            $("#button-config").hide();
            $("#button-session").removeClass("t-button-active");
          }
      });
      $("#drop-area").sortable(
      {
        containment: "parent",
        cursor: 'move',
        scrollSpeed: 300 
      });
  }

  /* 
      se não exister com o mouse clicado no input então verifica se os dados são válidos, 
      se não muda a cor da borda para vermelho,
      se sim então retorna ao normal

    */
  function input_check(e,id,regex)
  {
      var val = $(id).val();
      if(val.trim().length !== 0) 
      {
        if(!$(e.target).is(id))
        {
              if(!val.match(regex)) 
              {
                  $(id).addClass("input-error");
              }
              else
              {
                  $(id).removeClass("input-error");
              }
        }
      }
  }

  /* Verifica se a existe algum determinado input dentro das sessões que não está com os dados válidos. */
  $(document).mousedown(function(e)
  {
      input_check(e,"#back-change",back_regex);
      input_check(e,"#date-change",date_regex);
      input_check(e,"#url-change-1",url_regex);
      input_check(e,"#url-change-2",url_regex);
      input_check(e,"#url-change-3",url_regex);
      input_check(e,"#url-change-4",url_regex);
  });
  /* 
    Função que verifica os inputs e valida as os inputs necessários
    E então se não existe nenhum erro torna a div da sessão selecionada arrastável.
  */
  $(".session").mousemove(function()
  {
      var s_order= $(this).index()+1;
      var inputs_val = inputs_get(s_id+s_order);
      var enable=0;
      if(!inputs_empty(inputs_val))
      {
        if(s_order===1 && inputs_val[2].match(url_regex)) enable=1;
        if(s_order===2 && inputs_val[5].match(back_regex)) enable=1;
      }
      if(s_order===3)
      {
        if(inputs_val[0].match(date_regex) || !inputs_val[0].trim().length === 0)
        {
            if(inputs_val[1].match(url_regex) || inputs_val[2].match(url_regex) || inputs_val[3].match(url_regex)) 
            {
                enable=1;
            }
        }
      }
      if(enable==1)
      {
          session_drag(s_order);
      }
      else
      {
          $(this).draggable({disabled:true});
      }
  });

  /* 
    Função que vefica se uma sessão foi arrasta para área e envia um json com os dados da sessão 
    para a função new bloc, que faz uma requisão e retorna para a página.
    é verificado a ordem de sessão.
  */

  $("#drag-area").droppable({
    accept: ".session",
    drop: function(event, ui)
    {
      $(ui.helper).remove();
      $("#welcome").remove();
      $("#new-drop").show();
      var s_order= ui.draggable.index()+1;
      var inputs_val=0;
      inputs_val = inputs_get(s_id+s_order);
      if(s_order===1)
      {
          new_bloc(s_order,
          {
              title:inputs_val[0],
              subtitle:inputs_val[1],
              url:inputs_val[2]
          });
      }
      if(s_order===2)
      {
          new_bloc(s_order,
          {
              title:inputs_val[0],
              b_target:inputs_val[1],
              b_link:inputs_val[2],
              b_text:inputs_val[3],
              subtitle:inputs_val[4],
              background:inputs_val[5]
          });
      }
      if(s_order===3)
      {
          new_bloc(s_order,
          {
              date:inputs_val[0],
              url1:inputs_val[1],
              url2:inputs_val[2],
              url3:inputs_val[3]
          });
      }
    }
  });
  /* Efeito quando arrasta uma sessão adiciona
    uma classe que muda a cor das bordas da área para soltar 
  */
  $("#drag-area").on("dropover", function() 
  {
      $(".drag-here").addClass("drag-over");
  });
  $("#drag-area").on("dropout", function()
  {
      $(".drag-here").removeClass("drag-over");
  });
  /* botão da sessão 2 e suas configurações aparecem e sumam ao depender do click */
  $("#button-session").click(function()
  {
      $(this).toggleClass("t-button-active");
      $("#button-config").toggle();
  });
  /* função para deletar uma sessão arrastada */
  $(document).on('click', '.page-delete',  function()
  {
      $(this).parent(".page-session").remove();
  });
});