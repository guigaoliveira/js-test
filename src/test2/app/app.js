var fs = require("fs");
var express = require('express'), app = express();
var bodyParser = require('body-parser');
app.use(express.static(__dirname + '/public'));
app.listen(80, function() {
  console.log("Servidor On-line");
});
app.use(bodyParser());
app.post('/session_1', function(req, res) 
{
    	var title = req.body.title;
        var stitle = req.body.subtitle;
        var url= req.body.url;
        res.writeHead(200, { 'Content-Type': 'text/html' });  
		res.write('<div class="page-session">');    
		res.write('<div class="page-title">'+title+'</div>');  
		res.write('<div class="page-stitle">'+subtitle+'</div>');
		res.write('<img class="page-image" src="'+url+'"/>'); 
		res.write('<div class="page-delete">X</div>');    
		res.write('</div>');  
		res.end();  
});
app.post('/session_2', function(req, res) 
{
    	var title = req.body.title;
        var subtitle = req.body.subtitle;
        var background= req.body.background;
        var b_target = req.body.b_target;
        var b_link= req.body.b_link;
        var b_text = req.body.b_text;
        res.writeHead(200, { 'Content-Type': 'text/html' });  
		res.write('<div class="page-session" style="background:'+background+';">');    
		res.write('<div class="page-title">'+title+'</div>');  
		res.write('<div class="page-stitle">'+subtitle+'</div>');   
		res.write('<center><a target="'+b_target+'" href='+b_link+' class="tool-button page-button">'+b_text+'</a></center>');   
		res.write('<div class="page-delete">X</div>');
		res.write('</div>');  
		res.end();  
});
app.post('/session_3', function(req, res) 
{
    	var url1 = req.body.url1;
        var url2 = req.body.url2;
        var url3= req.body.url3;

        res.writeHead(200, { 'Content-Type': 'text/html' });  
		res.write('<div class="page-session">');    
		if(url1)
		{
			res.write('<img class="page-image" src="'+url1+'"/>');
		}
		if(url2)
		{
			res.write('<img class="page-image" src="'+url2+'"/>');
		}
		if(url3)
		{
			res.write('<img class="page-image" src="'+url3+'"/>');
		}
		res.write('</div>');  
		res.end();  
});
